<?php

declare(strict_types = 1);

namespace Drupal\Tests\scheduled_transitions_sidebar\Functional;

use Drupal\Core\Url;
use Drupal\entity_test\Entity\EntityTestMulRevPub;
use Drupal\scheduled_transitions\Entity\ScheduledTransition;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\user\UserInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Tests Scheduled Transitions Sidebar.
 *
 * @group scheduled_transitions_sidebar
 */
final class ScheduledTransitionsSidebarTest extends BrowserTestBase {

  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'scheduled_transitions_sidebar',
    'scheduled_transitions',
    'moderation_sidebar',
    'toolbar',
    'content_moderation',
    'workflows',
    'entity_test',
  ];

  /**
   * Workflow for testing.
   *
   * @var \Drupal\workflows\WorkflowInterface
   */
  protected WorkflowInterface $testWorkflow;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->testWorkflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($this->testWorkflow, 'entity_test_mulrevpub', 'entity_test_mulrevpub');
    $this->testUser = $this->createUser([
      'view test entity',
      'access toolbar',
      'access toolbar',
      'use ' . $this->testWorkflow->id() . ' transition create_new_draft',
      'use ' . $this->testWorkflow->id() . ' transition archive',
      'use ' . $this->testWorkflow->id() . ' transition publish',
      'use moderation sidebar',
    ]);
    $this->drupalLogin($this->testUser);
  }

  /**
   * Tests alter hook.
   */
  public function testSidebar() {
    $entity = EntityTestMulRevPub::create([
      'name' => $this->randomMachineName(),
    ]);
    $entity->save();

    $newState = 'published';
    $date = new \DateTime('2 Feb 2018 11am');
    $scheduledTransition = ScheduledTransition::create([
      'entity' => $entity,
      'entity_revision_id' => 1,
      'author' => $this->testUser,
      'workflow' => $this->testWorkflow->id(),
      'moderation_state' => $newState,
      'transition_on' => $date->getTimestamp(),
    ]);
    $scheduledTransition->save();

    $url = Url::fromRoute('moderation_sidebar.sidebar_latest', [
      'entity_type' => $entity->getEntityTypeId(),
      'entity' => $entity->id(),
    ]);
    $this->drupalGet($url);
    // From moderation_sidebar.module:
    $this->assertSession()->pageTextContains('Status: Draft');
    // From scheduled_transitions_sidebar.module:
    $this->assertSession()->pageTextContains('Scheduled transitions');
    $this->assertSession()->pageTextContains('Fri, 02/02/2018 - 11:00');
    $this->assertSession()->pageTextContains('Changing from Draft to Published');
    $this->assertSession()->linkExists('View content');
  }

}
