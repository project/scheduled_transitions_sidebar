<?php

declare(strict_types = 1);

namespace Drupal\scheduled_transitions_sidebar;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\scheduled_transitions\Entity\ScheduledTransitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hooks into hooks invoked by moderation_sidebar.module.
 */
final class ScheduledTransitionsModerationSidebarHooks implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new ScheduledTransitionsModerationSidebarHooks.
   */
  public function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
    private DateFormatterInterface $dateFormatter,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Implements hook_moderation_sidebar_alter().
   *
   * Adds scheduled transitions details to sidebar.
   *
   * @see \scheduled_transitions_sidebar_moderation_sidebar_alter()
   */
  public function moderationSidebarAlter(array &$build, EntityInterface $entity): void {
    $entityTypeId = $entity->getEntityTypeId();
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);

    $transitionStorage = $this->entityTypeManager->getStorage('scheduled_transition');
    $ids = $transitionStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity__target_type', $entityTypeId)
      ->condition('entity__target_id', $entity->id())
      ->sort('transition_on', 'ASC')
      ->execute();

    $rows = array_map(
      function (ScheduledTransitionInterface $scheduledTransition) use ($entityStorage) {
        $row = [];

        $workflowPlugin = $scheduledTransition->getWorkflow()->getTypePlugin();
        $workflowStates = $workflowPlugin ? $workflowPlugin->getStates() : [];

        // From...
        $entityRevisionId = $scheduledTransition->getEntityRevisionId();
        $entityRevision = $entityStorage->loadRevision($entityRevisionId);
        $revisionTArgs = ['@revision_id' => $entityRevisionId];
        if ($entityRevision) {
          $row['from_revision'] = $entityRevision->toLink($this->t('View content', $revisionTArgs), 'revision');
          $fromState = $workflowStates[$entityRevision->moderation_state->value] ?? NULL;
          $row['from_state'] = $fromState ? $fromState->label() : $this->t('- Missing from workflow/state -');
        }
        else {
          $row['from_revision'] = [
            // Span 'from_revision', 'from_state'.
            'colspan' => 2,
            'data' => $this->t('Deleted revision #@revision_id', $revisionTArgs),
          ];
        }

        // To.
        $toState = $workflowStates[$scheduledTransition->getState()] ?? NULL;
        $row['to_state'] = $toState ? $toState->label() : $this->t('- Missing to workflow/state -');

        // Date.
        $time = $scheduledTransition->getTransitionTime();
        $row['date'] = $this->dateFormatter->format($time);

        return $row;
      },
      $transitionStorage->loadMultiple($ids)
    );

    if (count($rows) > 0) {
      // Move to actions.secondary until issue is merged:
      // https://www.drupal.org/project/moderation_sidebar/issues/3211572.
      $build['actions']['secondary'] = [
        '#theme' => 'scheduled_transitions_sidebar_transitions',
        '#transitions' => $rows,
      ];
    }

    $cacheability = (new CacheableMetadata())
      ->addCacheTags($this->entityTypeManager->getDefinition('scheduled_transition')->getListCacheTags())
      ->addCacheableDependency($entity);
    $cacheability->applyTo($build);
  }

}
