<?php

/**
 * @file
 * Provides hooks for Scheduled Transitions + Moderation Sidebar integration.
 */

declare(strict_types = 1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\scheduled_transitions_sidebar\ScheduledTransitionsModerationSidebarHooks as Hooks;

/**
 * Implements hook_moderation_sidebar_alter().
 *
 * @see \Drupal\scheduled_transitions_sidebar\ScheduledTransitionsModerationSidebarHooks::moderationSidebarAlter
 */
function scheduled_transitions_sidebar_moderation_sidebar_alter(array &$build, EntityInterface $entity): void {
  $moderationSidebarHooks = \Drupal::classResolver()->getInstanceFromDefinition(Hooks::class);
  assert($moderationSidebarHooks instanceof Hooks);
  $moderationSidebarHooks->moderationSidebarAlter($build, $entity);
}

/**
 * Implements hook_theme().
 */
function scheduled_transitions_sidebar_theme(array $existing, string $type, string $theme, string $path): array {
  return [
    'scheduled_transitions_sidebar_transitions' => [
      'variables' => [
        'transitions' => [],
      ],
    ],
  ];
}

/**
 * Template preprocessor for 'scheduled_transitions_sidebar_transitions'.
 *
 * @param array $variables
 *   Variables.
 */
function template_preprocess_scheduled_transitions_sidebar_transitions(array &$variables) {
  $variables['#attached']['library'][] = 'scheduled_transitions_sidebar/transition_list';
}
